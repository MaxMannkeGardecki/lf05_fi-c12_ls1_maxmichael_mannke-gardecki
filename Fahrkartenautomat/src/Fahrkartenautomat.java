﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag;
       double eingezahlterGesamtbetrag;
       
       do { //Bestellung erfassen
    	   zuZahlenderBetrag = fahrkartenbestellungErfassen();
    	    //eingezahlten Betrag erfassen
    	   eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
    	   
    	   warte(3450);
       }
       
       while(true);
       
    }  
       
    private static void warte(int milisecond){  
    	try {
    		Thread.sleep(milisecond);
    	} catch (InterruptedException e){
          e.printStackTrace();
    	}  
     }
    
    private static void münzeAusgeben (int betrag, String einheit) {
    	System.out.println(betrag + " " + einheit);
    }
    
    private static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag = 0;
    	int tickets;
    	boolean ticketInputCheck = true;
    	int ticketChoice = 0;
    			
    	System.out.println("Bitte wählen Sie Ihre Wunschfahrkarte für Berlin AB aus \n" +
    					   " Einzelfahrschein Regeltarif AB [€ 3,00] (1)\n" + 
    					   " Tageskarte Regeltarif AB [€ 8,80] (2)\n" + 
    					   " Kleingruppen Tageskarte Regeltarif AB [€ 25,50] (3)\n\n");
    	do {
    		ticketChoice = tastatur.nextInt();
    		System.out.println("Ihre Wahl: " + ticketChoice);
    		if((ticketChoice>0) && (ticketChoice<4))
    			ticketInputCheck = false;
    		else
    			System.out.println(" FALSCHE EINGABE ");
    		
    	}
    	
    	while(ticketInputCheck);
    	
    	System.out.println("Anzahl der Tickets: ");
    	tickets = tastatur.nextInt();
    	
    	if ((tickets>10) || (tickets<1)) {
    		tickets = 1;
    		System.out.println("Ungültige Anzahl an Tickets. Bestellung wird für (1) Ticket fortgeführt!");
    	}
    	
    	switch(ticketChoice) {
    		case 1: zuZahlenderBetrag = 3.00 * tickets; break;
    		case 2: zuZahlenderBetrag = 8.80 * tickets; break;
    		case 3: zuZahlenderBetrag = 25.50 * tickets; break;
    	}
    	
    	return zuZahlenderBetrag;
    }
    
    private static double fahrkartenBezahlen(double zuZahlenderBetrag) {
       Scanner tastatur = new Scanner(System.in);
       
       double eingeworfeneMünze;
       double eingezahlterGesamtbetrag = 0.0;
       
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen: " + "%.2f€\n" , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 0,05€, höchstens 2,00€): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }
 
     return eingezahlterGesamtbetrag;
    }
    
    
     private static void fahrkartenAusgeben() {
    	 
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          warte(250);
		}
       
       System.out.println("\n\n");
     }
     
     private static void rückgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.printf("Der Rückgabebetrag in Höhe von " + "%.2f€\n", rückgabebetrag );
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.00f) // 2 EURO-Münzen
           {
        	  System.out.println("2,00€");
	          rückgabebetrag -= 2.00f;
           }
           while(rückgabebetrag >= 1.00f) // 1 EURO-Münzen
           {
        	  System.out.println("1,00€");
	          rückgabebetrag -= 1.00f;
           }
           while(rückgabebetrag >= 0.50f) // 50 CENT-Münzen
           {
        	  System.out.println("0,50€");
	          rückgabebetrag -= 0.50f;
           }
           while(rückgabebetrag >= 0.20f) // 20 CENT-Münzen
           {
        	  System.out.println("0,20€");
 	          rückgabebetrag -= 0.20f;
           }
           while(rückgabebetrag >= 0.10f) // 10 CENT-Münzen
           {
        	  System.out.println("0,10€");
	          rückgabebetrag -= 0.10f;
           }
           while(rückgabebetrag >= 0.05f)// 5 CENT-Münzen
           {
        	  System.out.println("0,05€");
 	          rückgabebetrag -= 0.05f;
 	          
 	       
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       
     }  
    }


//	5. Ich habe "int" für die Anzahl der Tickets gewählt, da es sich um eine Ganzzahl handelt und eine Bestellung
//     von über 2 147 483 648 Tickets unwahrscheinlich ist.
//  6. Bei der Methode wird der "zuZahlenderBetrag"-float mit dem "anzahlTickets"-int multipliziert und als Summe ausgegeben.
