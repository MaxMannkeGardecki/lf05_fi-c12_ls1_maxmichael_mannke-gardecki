
import java.util.ArrayList;

/**
 * Raumschiff-Klasse
 * @author Mannke-Gardecki
 * @since 18.05.2022
 */
public class Raumschiff {
	private int photonenTorpedoAnzahl;
	private int energieversorgungInProzent;
	private int hülleInProzent;
	private int schildeInProzenz;
	private int lebenserhaltungInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	
	public Raumschiff() {
		
	}
	/**
	 * 
	 * @param photonenTorpedoAnzahl
	 * @param energieversorgungInProzent
	 * @param hülleInProzent
	 * @param schildeInProzenz
	 * @param lebenserhaltungInProzent
	 * @param androidenAnzahl
	 * @param schiffsname
	 */
	public Raumschiff(int photonenTorpedoAnzahl, int energieversorgungInProzent, int hülleInProzent,
			int schildeInProzenz, int lebenserhaltungInProzent, int androidenAnzahl, String schiffsname) {
		
		this.photonenTorpedoAnzahl = photonenTorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.hülleInProzent = hülleInProzent;
		this.schildeInProzenz = schildeInProzenz;
		this.lebenserhaltungInProzent = lebenserhaltungInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}
/**
 * 
 * @return int
 */
	public int getPhotonenTorpedoAnzahl() {
		return photonenTorpedoAnzahl;
	}
/**
 * 
 * @param photonenTorpedoAnzahl
 */
	public void setPhotonenTorpedoAnzahl(int photonenTorpedoAnzahl) {
		this.photonenTorpedoAnzahl = photonenTorpedoAnzahl;
	}
/**
 * 
 * @return int
 */
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
/**
 * 
 * @param energieversorgungInProzent
 */
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
/**
 * 
 * @return int
 */
	public int getHülleInProzent() {
		return hülleInProzent;
	}
	/**
	 * 
	 * @param hülleInProzent
	 */
	public void setHülleInProzent(int hülleInProzent) {
		this.hülleInProzent = hülleInProzent;
	}
	/**
	 * 
	 * @return int
	 */
	public int getSchildeInProzenz() {
		return schildeInProzenz;
	}
	/**
	 * 
	 * @param schildeInProzenz
	 */
	public void setSchildeInProzenz(int schildeInProzenz) {
		this.schildeInProzenz = schildeInProzenz;
	}
	/**
	 * 
	 * @return int
	 */
	public int getLebenserhaltungInProzent() {
		return lebenserhaltungInProzent;
	}
	/**
	 * 
	 * @param lebenserhaltungInProzent
	 */
	public void setLebenserhaltungInProzent(int lebenserhaltungInProzent) {
		this.lebenserhaltungInProzent = lebenserhaltungInProzent;
	}
	/**
	 * 
	 * @return int
	 */
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
	/**
	 * 
	 * @param androidenAnzahl
	 */
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	/**
	 * 
	 * @return String
	 */
	public String getSchiffsname() {
		return schiffsname;
	}
	/**
	 * 
	 * @param schiffsname
	 */
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	public String toString() {
		
		
		return "Schiffsname:	" + schiffsname + "\n";
				
	}
	
	
}
	