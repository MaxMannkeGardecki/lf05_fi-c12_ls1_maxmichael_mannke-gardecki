/**
 * Ladungs-Klasse
 * @author Mannke-Gardecki
 * @since 18.05.2022
 */
public class Ladung {
	
	private String bezeichnung;
	private int menge;
	
	public Ladung () {
		
	}
	/**
	 * 
	 * @param bezeichnung
	 * @param menge
	 */
	public Ladung(String bezeichnung, int menge) {
		
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	/**
	 * 
	 * @return String
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}
	/**
	 * 
	 * @param bezeichnung
	 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	/**
	 * 
	 * @return int
	 */
	public int getMenge() {
		return menge;
	}
	/**
	 * 
	 * @param menge
	 */
	public void setMenge(int menge) {
		this.menge = menge;
	}
	
	
	

}
