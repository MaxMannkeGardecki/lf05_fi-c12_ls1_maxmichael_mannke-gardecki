import java.util.ArrayList;

public class Auto {

	private ArrayList<Ladung> ladungen;
	private String marke;
	private String farbe;
	
	public Auto() {
		
		System.out.println("neues Auto");
	}
	
	public Auto(String marke, String farbe) {
		
		this.marke = marke;
		this.farbe = farbe;
		
	}
	
	public Auto(ArrayList<Ladung> ladungen, String marke, String farbe) {
		
		this.marke = marke;
		this.farbe = farbe;
		
	}	
	
	public void setMarke(String marke) {
		this.marke = marke;
		
	}
	
	public String getMarke() {
		return this.marke;
		
	}
	
	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}
	
	public String getFarbe() {
		return this.farbe;
	}
	
	
	public void fahre(int Strecke) {
		System.out.println("Los gehts !!!");
		
	}
	
	public void tanke(int liter) {
		System.out.println("Bin getankt!");
	}
	
}
