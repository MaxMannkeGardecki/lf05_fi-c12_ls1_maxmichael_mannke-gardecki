
public class Aufgabe1 {

	public static void main(String[] args) {
		System.out.print("Das ist ein Beispielsatz. ");
		System.out.println("Ein Beispielsatz ist das.");
		System.out.print("Das ist ein \"Beispielsatz\"\n"+"Ein Beispielsatz ist das.");
		
		//Der Unterschied zwischen der print und println Anweisung ist, dass bei der
		//println Anweisung ein Zeilenumbruch stattfindet

	}

}
